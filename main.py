import falcon
from wsgiref.simple_server import make_server
import ee

from datetime import datetime,timedelta
import time
import ast
from pyasn1.type.univ import Null
import wget
import urllib
import json
import multiprocessing
pool = multiprocessing.Pool()


class getAlertPNG:
    ee.Initialize()
    geeGeometry = ''
    featureArea = 0
    def linear2perc(self,image,region):
        region = region
        reducer2 = ee.Reducer.percentile([2])
        reducer98 = ee.Reducer.percentile([98])
        min = image.reduceRegion(reducer=reducer2,geometry=region,maxPixels=1e10,scale=60)
        max = image.reduceRegion(reducer=reducer98,geometry=region,maxPixels=1e10,scale=60)
        r = image.select('r').unitScale(min.get('r'), max.get('r')).multiply(255).round().byte()
        g = image.select('g').unitScale(min.get('g'), max.get('g')).multiply(255).round().byte()
        b = image.select('b').unitScale(min.get('b'), max.get('b')).multiply(255).round().byte()
        imageEnhanced = ee.Image.cat(r, g, b).visualize().rename(['r', 'g', 'b'])
        return ee.Image(imageEnhanced.copyProperties(image))

    def SENTINEL2(self,alert_id,alert_geometry_id,date_detection,feature):
        featureArea = ee.Number(feature.area()).sqrt().getInfo()*3
        print("featureArea calcs:"+str(featureArea))
        date_detection = date_detection.strip()
        upper_date_range = datetime.fromisoformat(date_detection) + timedelta(days=10)
        upper_date_range = upper_date_range.strftime('%Y-%m-%d')
        lower_date_range = datetime.fromisoformat(date_detection) #- timedelta(days=10)
        lower_date_range = lower_date_range.strftime('%Y-%m-%d')
        s2Collection = ee.ImageCollection("COPERNICUS/S2_SR").filterBounds(feature).filterDate(lower_date_range,upper_date_range)
        s2Clouds = ee.ImageCollection('COPERNICUS/S2_CLOUD_PROBABILITY')
        s2SrWithCloudMask = ee.Join.saveFirst('cloud_mask').apply(s2Collection,s2Clouds,ee.Filter.equals('system:index',None,'system:index'))
        def maskClouds10(img) :
            clouds = ee.Image(img.get('cloud_mask')).select('probability')
            isNotCloud = clouds.lt(20)
            return img.updateMask(isNotCloud)
        s2CloudMasked = ee.ImageCollection(s2SrWithCloudMask)
        imageCollection = s2CloudMasked.filterBounds(feature).filterDate(lower_date_range,upper_date_range).sort('system:index').map(maskClouds10)
        #imageCollection = ee.ImageCollection("COPERNICUS/S2_SR").filterBounds(feature).filterDate(lower_date_range,upper_date_range).sort('system:index').limit(4)
        firstImageID = imageCollection.first().get("system:index").getInfo()
        firstImageDate = imageCollection.first().get("system:time_start").getInfo()
        firstImageDate = ee.Date(firstImageDate).format('YYYY-MM-dd').getInfo()
        #print("firstImageID = "+str(firstImageID))
        image = imageCollection.median().select(['B11', 'B8', 'B4'], ['r', 'g', 'b'])
        print("Before Linear2Perc")
        image = self.linear2perc(image, feature.buffer(featureArea+1))
        print("After Linear2Perc")
        #image = imageCollection.median().visualize(['B11','B8','B4'],None,None,2,4000,0.95,1,None,True)
        #featureArea = ee.Number(feature.area()).sqrt().getInfo()*3
        featureImage = ee.FeatureCollection([feature]).style(color='red',fillColor='00000000',width=3)
        featureImage = featureImage#.blend(centroidImage)
        preparedImage = image.blend(featureImage).getThumbURL({
                'name':str(alert_id)+'_'+str(alert_geometry_id)+'_'+firstImageID,
                'bands':['r','g','b'],
                'min:': 0,'max':255, #[0.08, 0.07, 0.2],
                'dimensions':[640,640],
                'region':feature.buffer(featureArea+1),
                'format ':'png'
                })
        return {'link':preparedImage,'name':str(firstImageID), 'date':str(firstImageDate),'type':'Weekly Mosaic'}
    
    def SENTINEL2_MOSAIC(self,alert_id,alert_geometry_id,date_detection,feature):
        featureArea = ee.Number(feature.area()).sqrt().getInfo()*3
        year = int(datetime.fromisoformat(date_detection.strip()).year)
        lower_date_range = str(year-1)+'-01-01'
        upper_date_range = str(year-1)+'-12-29'
        s2Collection = ee.ImageCollection("COPERNICUS/S2_SR").filterBounds(feature).filterDate(lower_date_range,upper_date_range)
        s2Clouds = ee.ImageCollection('COPERNICUS/S2_CLOUD_PROBABILITY');
        s2SrWithCloudMask = ee.Join.saveFirst('cloud_mask').apply(s2Collection,s2Clouds,ee.Filter.equals('system:index',None,'system:index'))
        def maskClouds10(img) :
            clouds = ee.Image(img.get('cloud_mask')).select('probability');
            isNotCloud = clouds.lt(20);
            return img.updateMask(isNotCloud)
            
        imageCollection = ee.ImageCollection(s2SrWithCloudMask).filterBounds(feature).filterDate(lower_date_range,upper_date_range).map(maskClouds10).median()
        image = imageCollection.select(['B11', 'B8', 'B4'], ['r', 'g', 'b'])
        print("Before Linear2Perc")
        image = self.linear2perc(image, feature.buffer(featureArea+1))
        #image = image.visualize(['B11','B8','B4'],None,None,2,4000,0.95,1,None,True)
        firstImageID = 'MOSAIC'
        #print("firstImageID = "+str(firstImageID))
        #S2 visualization PARAMS
        #featureArea = ee.Number(feature.area()).sqrt().getInfo()*3
        featureImage = ee.FeatureCollection([feature]).style(color='red',fillColor='00000000',width=3)
        featureImage = featureImage#.blend(centroidImage)
        preparedImage = image.blend(featureImage).getThumbURL({
                'name':str(alert_id)+'_'+str(alert_geometry_id)+'_'+firstImageID,
                'bands':['r','g','b'],
                'min:': 0,'max':255,
                'dimensions':[640,640],
                'region':feature.buffer(featureArea+1),
                'format ':'png'
                })
        return {'link':preparedImage,'name':str(firstImageID), 'date':str(lower_date_range),'type':'Annual Mosaic' }
    
    
    def on_post(self, req, resp):
        #print( req.params)
        raw_data = json.load(req.bounded_stream)
        #print(raw_data.get('alert_id'))
        polygon = json.loads(raw_data.get('polygon'))
        alert_id = raw_data.get('alert_id')
        alert_geometry_id = raw_data.get('alert_geometry_id')
        date_detection = raw_data.get('date_detection')
        try:
            self.geeGeometry = ee.Geometry.MultiPolygon(polygon['coordinates'],'EPSG:4326',True)
            self.featureArea = ee.Number(self.geeGeometry.area(1)).sqrt().getInfo()*2.5
        except Exception:
            print('Check the geojson geometry consistency')
        #feature = ee.Geometry(geom,'EPSG:4326',True)
        
        linkS2 = self.SENTINEL2(alert_id,alert_geometry_id,date_detection,self.geeGeometry)
        linkS2Mosaico = self.SENTINEL2_MOSAIC(alert_id,alert_geometry_id,date_detection,self.geeGeometry)
        #print(linkS2Mosaico)
        imageThumbLinks = {'after':linkS2,'before':linkS2Mosaico}
        resp.status = falcon.HTTP_200 
        resp.media = imageThumbLinks
        
class getThumbs:
    ee.Initialize()
    geeGeometry = ''
    featureArea = 0

    def linear2perc(self,image,region):
        region = region
        reducer2 = ee.Reducer.percentile([2])
        reducer98 = ee.Reducer.percentile([98])
        min = image.reduceRegion(reducer=reducer2,geometry=region,maxPixels=1e10,scale=60)
        max = image.reduceRegion(reducer=reducer98,geometry=region,maxPixels=1e10,scale=60)
        r = image.select('r').unitScale(min.get('r'), max.get('r')).multiply(255).round().byte()
        g = image.select('g').unitScale(min.get('g'), max.get('g')).multiply(255).round().byte()
        b = image.select('b').unitScale(min.get('b'), max.get('b')).multiply(255).round().byte()
        imageEnhanced = ee.Image.cat(r, g, b).visualize().rename(['r', 'g', 'b'])
        return ee.Image(imageEnhanced.copyProperties(image))

    def imageToThumb(self,img,imageBands,imageOrder):
        eeMin = 0.02
        eeMax = 0.55
        gain = None
        gama = 1
        sat = 'L8'
        if("LANDSAT" in img):
            sat = 'L8'
            eeimg = ee.Image(img).select([1,2,3,4,5,6],['blue','green','red','nir','swir1','swir2'])
            gain = None
            gama = 1.15
        if("S2_SR" in img):
            sat = 'S2'
            eeimg = ee.Image(img).select([1,2,3,8,11,12],['blue','green','red','nir','swir1','swir2'])
            gain = None
            gama = 0.80
        if("S1_GRD" in img):
            sat = 'S1'
            eeimg = ee.Image(img).select(['VH','VH','VH','VH','VH','VH'],['blue','green','red','nir','swir1','swir2'])
        if("S2" in img):
            # print("S2")
            eeMin = 0
            eeMax = 5000
        if(imageBands == 'ndvi,' and "S1_GRD" not in img):
            eeMin = 0.1
            eeMax = 0.8
            gain = None
            gama = 1
            if("S2_SR" in img):
                gain = None
                gama = 1
                eeMin = -0.15
                eeMax = 0.9
            imageBands = ['NDVI','NDVI','NDVI']
            eeimg = eeimg.normalizedDifference(['nir','red']).rename('NDVI')
        else:
            if(imageBands == 'red,green,blue'):
                eeMin = 0.04
                eeMax = 0.25
                if("S2" in img):
                    # print("S2")
                    eeMin = 415
                    eeMax = 1374
            imageBands = imageBands.split(',')
        if("S1_GRD" in img):
            # print("S1")
            eeMin = -29
            eeMax = -12
        
        visGeomR = ee.Image(0).toByte().paint(self.geeGeometry,255,4)
        visGeomG = ee.Image(0).toByte()
        visGeomB = ee.Image(0).toByte()
        imageGeom = ee.Image.rgb(visGeomR.rename('R'),visGeomG.rename('G'),visGeomB.rename('G')).updateMask(visGeomR.eq(255))
        geometryIMG = ee.Feature(self.geeGeometry).getMapId({'color':'red'})['image']

        buffer = self.featureArea
        if(self.featureArea < 800):
            #print('Area Antes: ',self.featureArea)
            buffer = self.featureArea * 1.8
            #print('Area Depois: ',self.featureArea)
        if(self.featureArea < 400):
            buffer = self.featureArea * 3.8
        #print('Area'+str(self.featureArea));
        #print(eeMin)
        #print(eeMax)
        #print(gain)
        #print(gama)
        image = eeimg.select(imageBands, ['r', 'g', 'b'])
        image = self.linear2perc(image, self.geeGeometry.buffer(buffer+1)).blend(imageGeom)
        #imageThumb = eeimg.visualize(imageBands,gain,None,eeMin,eeMax,gama).blend(imageGeom)
        imageThumb = image.getThumbURL({
            'name':img,
            'bands':['r','g','b'],#imageBands,
            'min':0,
            'max':255,
            'dimensions':[320,320],
            'region':self.geeGeometry.centroid(1).buffer(buffer).bounds(1),
            'format':'png'
        })
        mapid = image.getMapId({'bands': ['r','g','b'], 'min': 0, 'max': 255})
        #mapid = eeimg.getMapId({'bands': imageBands, 'min': eeMin, 'max': eeMax,'gain':gain,'gamma':gama})
        date = ee.Image(img).get('DATE_ACQUIRED').getInfo()
	#imageOrder = int(datetime.datetime.strptime(date, '%d/%m/%Y').strftime("%s"))
        if("COPERNICUS" in img):
            date = datetime.fromtimestamp(ee.Image(img).get('system:time_start').getInfo()/1000).strftime("%Y-%m-%d")   
        imageOrder = -int(datetime.strptime(date, '%Y-%m-%d').strftime("%s"))
        return {'order':imageOrder,'thumb' : imageThumb, 'tile' : mapid['tile_fetcher'].url_format, 'imgID':img,'date':date,'imgSat':sat}

    def on_post(self, req, resp):
        imageName  = req.get_param('image',False)
        imageOrder = req.get_param('order',False)
        imageBands = req.get_param('bands',False)
        polygon = json.loads(req.get_param('polygon',False)  or req.params['polygon'])  #get via GET or POST
        self.geeGeometry = ee.Geometry.MultiPolygon(polygon['coordinates'],'EPSG:4326',True)
        self.featureArea = ee.Number(self.geeGeometry.area(1)).sqrt().getInfo()*2.5
        print('Area,',self.featureArea)
        imageThumbLink = self.imageToThumb(imageName,imageBands,imageOrder)
        resp.status = falcon.HTTP_200
        resp.media = imageThumbLink

    # def on_get(self, req, resp):
    #     imageName = req.get_param('image',False)
    #     polygon = json.loads(req.get_param('polygon',False)  or req.params['polygon'])  #get via GET or POST
    #     self.geeGeometry = ee.Geometry.MultiPolygon(polygon['coordinates'],'EPSG:4326',True)
    #     self.featureArea = ee.Number(self.geeGeometry.area()).sqrt().getInfo()
    #     self.imageToThumb(imageName) 
    #     resp.status = falcon.HTTP_200 
    #     resp.media = 'quote'

class getImageList:
    ee.Initialize()
    landsat8 = ee.ImageCollection("LANDSAT/LC08/C02/T1_TOA")
    sentinel2 = ee.ImageCollection("COPERNICUS/S2_SR")
    sentinel1 = ee.ImageCollection("COPERNICUS/S1_GRD")
    EE_TILES = 'https://earthengine.googleapis.com/map/{mapid}/{{z}}/{{x}}/{{y}}?token={token}'
    geeGeometry = ''
    featureArea = 0
    
    def on_post(self, req, resp):
        """Handles POST requests"""
        resp.status = falcon.HTTP_200 
        print(req.params)
        polygon = json.loads(req.get_param('polygon',False)  or req.params['polygon'])  #get via GET or POST
        timeFrame = json.loads(req.get_param('tempo',False)  or req.params['tempo'])
        satellite = req.get_param('satellite',False)  or req.params['satellite']
        dateAfter = req.get_param('date',False)  or req.params['date']
        print("date"+str(dateAfter))
        if(timeFrame == 0):
                dateBefore = (datetime.fromisoformat(dateAfter.strip())  - timedelta(days=(60))).strftime("%Y-%m-%d")   
                dateAfter = (datetime.fromisoformat(dateAfter.strip()) + timedelta(days=(20))).strftime("%Y-%m-%d")
        else:
                dateBefore = (datetime.fromisoformat(dateAfter.strip()) - timedelta(days=(60+(60*timeFrame)))).strftime("%Y-%m-%d")
                dateAfter = (datetime.fromisoformat(dateAfter.strip()) - timedelta(days=(60*timeFrame))).strftime("%Y-%m-%d")
        
        if(polygon != ''):
            self.geeGeometry = ee.Geometry.MultiPolygon(polygon['coordinates'],'EPSG:4326',True)
            self.featureArea = ee.Number(self.geeGeometry.area()).sqrt().getInfo()
            if('All' in satellite):
                landsatListL8 = self.landsat8.filterDate(dateBefore,dateAfter).filterBounds(self.geeGeometry).limit(20,'system:time_end',False) #.aggregate_array('system:id').getInfo()
                landsatListS2 = self.sentinel2.filterDate(dateBefore,dateAfter).filterBounds(self.geeGeometry).limit(20,'system:time_end',False)#.aggregate_array('system:id').getInfo()
                landsatListS1 = self.sentinel1.filterDate(dateBefore,dateAfter).filterBounds(self.geeGeometry).limit(20,'system:time_end',False)#.aggregate_array('system:id').getInfo()
                landsatList = landsatListL8.merge(landsatListS2).merge(landsatListS1).limit(300,'system:time_end',False).aggregate_array('system:id').getInfo()
                #landsatList = [*landsatListL8,*landsatListS2,*landsatListS1]
            if('optico' in satellite):
                landsatListL8 = self.landsat8.filterDate(dateBefore,dateAfter).filterBounds(self.geeGeometry).limit(20,'system:time_end',False)
                landsatListS2 = self.sentinel2.filterDate(dateBefore,dateAfter).filterBounds(self.geeGeometry).limit(20,'system:time_end',False)
                landsatList = landsatListL8.merge(landsatListS2).limit(300,'system:time_end',False).aggregate_array('system:id').getInfo()
                #landsatList = [*landsatListL8,*landsatListS2]
            if('L8' in satellite):
                 landsatList = self.landsat8.filterDate(dateBefore,dateAfter).filterBounds(self.geeGeometry).limit(100,'system:time_end',False).aggregate_array('system:id').getInfo()
            if('S2' in satellite):
                landsatList = self.sentinel2.filterDate(dateBefore,dateAfter).filterBounds(self.geeGeometry).limit(100,'system:time_end',False).aggregate_array('system:id').getInfo()
            if('S1' in satellite):
                landsatList = self.sentinel1.filterDate(dateBefore,dateAfter).filterBounds(self.geeGeometry).limit(100,'system:time_end',False).aggregate_array('system:id').getInfo()
            #landsatList = list(map(self.imageToThumb,landsatList))
            resp.media = landsatList
        else:
            quote = {
                'quote': (
                    "I've always been more interested in "
                    "the future than in the past."
                ),
                'author': 'Grace Hopper'
            }
            resp.media = quote
#api = falcon.API()
api = falcon.App(cors_enable=True)
api.req_options.auto_parse_form_urlencoded=True
api.req_options.auto_parse_form_urlencoded = True #Enable POST parameters receive
ee.Initialize()
api.add_route('/getImageList', getImageList())
api.add_route('/getThumb', getThumbs())
api.add_route('/createPNG', getAlertPNG())

if __name__ == '__main__':
    with make_server('', 9000, api) as httpd:
        print('Serving on port 9000...')

        # Serve until process is killed
        httpd.serve_forever()
