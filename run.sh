gunicorn -b 0.0.0.0 -t 3000 --capture-output --access-logfile access.log --error-logfile error.log --threads 8 --workers 6 --worker-class gevent --worker-connections 10000 -k sync main:api
